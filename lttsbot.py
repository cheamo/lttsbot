# import all necessary commands and libraries
import discord
import asyncio
import logging
import simplejson as json
import pyz3r
import random
from discord.ext.commands import Bot
from constants import *
from difficulty_ratings import get_difficulty, MAX_NORMALIZED_DIFFICULTY

with open('botconfig.json') as botconfig:
    botconfig = json.load(botconfig)

# Define discord client
client = Bot(command_prefix=botconfig['clientid'])

# Option arrays
option_difficulty_all = [ EASY, NORMAL, HARD, EXPERT, INSANE, CROWD_CONTROL ]
option_difficulty = [ EASY, NORMAL, HARD ]
option_enemizer = [ True, False ]
option_logic_all = [ NO_GLITCHES, OVERWORLD_GLITCHES, MAJOR_GLITCHES, NONE ]
option_logic = [ NO_GLITCHES, OVERWORLD_GLITCHES ]
option_mode = [ STANDARD, OPEN, INVERTED ]
option_spoilers = [ True, False ] 
option_tournament = [ True, False ]
option_variation_all = [ NONE, KEY_SANITY, RETRO, TIMED_RACE, TIMED_OHKO, OHKO ]
option_variation = [ NONE, KEY_SANITY ]
option_weapons = [ RANDOMIZED, UNCLE, SWORDLESS ]

@client.event
async def on_ready():
    print('Logged in as {}'.format(client.user.name))
    print('--------')


# !seed command
@client.event
async def on_message(message):
    if message.content.startswith('!seed'):
        channel = message.channel
        difficulty = random.choice(option_difficulty)
        logic = random.choice(option_logic)
        mode = random.choice(option_mode)
        variation = random.choice(option_variation)
        weapons = random.choice(option_weapons)
        settings = {
            DIFFICULTY: difficulty,
            ENEMIZER: False,
            LOGIC: logic,
            MODE: mode,
            SPOILERS: False,
            TOURNAMENT: False,
            VARIATION: variation,
            WEAPONS: weapons,
            LANGUAGE: "en"
        }
        seed = pyz3r.alttpr(
            randomizer='item', # optional, defaults to item
            settings=settings
        )
        seedhint = ""
        if "boots" in message.content:
            for area in seed.data['spoiler']:
                for location in seed.data['spoiler'][area]:
                    try:
                        item = seed.data['spoiler'][area][location]
                        if item == "PegasusBoots":
                            bootlocation = location
                            #print(location)
                            #print(item)
                    except Exception as e:
                        pass
                        #print(e)
            seedhint = "\n[ Boots location: {} ]".format(bootlocation)

        seedurl = seed.url
        seedcode = seed.code()
        seedoptions = "[ {} | {} | {} | {} | {} ]".format(difficulty, logic, mode, variation, weapons)
        seedhash = "[ {hash} ]".format(hash = ' | '.join(seedcode))
        seed_difficulty = "[ Difficulty: " + str(get_difficulty(settings)) + " / " + str(MAX_NORMALIZED_DIFFICULTY) + " ]"
        seedmessage = "{}\n{}\n{}{}\n{}".format(seedurl, seedoptions, seedhash, seedhint, seed_difficulty)
        await channel.send(seedmessage)
        print("Generated seed\n{}\n".format(seedmessage))

# Start the bot
client.run(botconfig['discordtoken'])
