from constants import *

difficulty = {
    EASY: 0,
    NORMAL: 1,
    HARD: 3
}

logic = {
    NO_GLITCHES: 0,
    OVERWORLD_GLITCHES: 1
}

mode = {
    STANDARD: 0,
    OPEN: 0,
    INVERTED: 3
}

variation = {
    NONE: 0,
    KEY_SANITY: 1
}

weapons = {
    RANDOMIZED: 0,
    UNCLE: 0,
    SWORDLESS: 3
}

MIN_NORMALIZE_DIFFICULTY = 1
MAX_NORMALIZED_DIFFICULTY = 5
MAX_DIFFICULTY = max(difficulty.values()) + max(logic.values()) + max(mode.values()) + max(variation.values()) + max(
    weapons.values())


def get_difficulty(settings):
    difficult_score = difficulty[settings[DIFFICULTY]] + \
                      logic[settings[LOGIC]] + \
                      mode[settings[MODE]] + \
                      variation[settings[VARIATION]] + \
                      weapons[settings[WEAPONS]]

    return round(difficult_score / MAX_DIFFICULTY * (
                MAX_NORMALIZED_DIFFICULTY - MIN_NORMALIZE_DIFFICULTY), 1) + MIN_NORMALIZE_DIFFICULTY
