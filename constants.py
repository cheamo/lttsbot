EASY = "easy"
NORMAL = "normal"
HARD = "hard"
EXPERT = "expert"
INSANE = "insane"
CROWD_CONTROL = "crowdControl"

NO_GLITCHES = "NoGlitches"
OVERWORLD_GLITCHES = "OverworldGlitches"
MAJOR_GLITCHES = "MajorGlitches"
NONE = "None"

STANDARD = "standard"
OPEN = "open"
INVERTED = "inverted"

NONE = "none"
KEY_SANITY = "key-sanity"
RETRO = "retro"
TIMED_RACE = "timed-race"
TIMED_OHKO = "timed-ohko"
OHKO = "ohko"

RANDOMIZED = "randomized"
UNCLE = "uncle"
SWORDLESS = "swordless"


LANGUAGE = "lang"
WEAPONS = "weapons"
VARIATION = "variation"
TOURNAMENT = "tournament"
SPOILERS = "spoilers"
MODE = "mode"
LOGIC = "logic"
ENEMIZER = "enemizer"
DIFFICULTY = "difficulty"